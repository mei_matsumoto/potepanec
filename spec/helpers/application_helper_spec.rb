require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full title" do
    subject { helper.full_title(page_title: page_title) }

    context "ページタイトルを指定する場合" do
      let(:page_title) { "test_title" }

      it { is_expected.to eq("test_title - BIGBAG Store") }
    end

    context "ページタイトルの指定がnilの場合" do
      let(:page_title) { nil }

      it { is_expected.to eq("BIGBAG Store") }
    end

    context "ページタイトルの指定が空白の場合" do
      let(:page_title) { "" }

      it { is_expected.to eq("BIGBAG Store") }
    end

    context "ページタイトルに引数がない場合" do
      it { expect(helper.full_title).to eq("BIGBAG Store") }
    end
  end
end
