require 'rails_helper'

RSpec.describe "PotepanProducts", type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_product_path(product.id)
  end

  describe "#show" do
    it "リクエストが成功すること" do
      expect(response).to have_http_status(200)
    end

    it "商品情報が表示されていること" do
      aggregate_failures do
        expect(response.body).to include product.name
        expect(response.body).to include product.description
        expect(response.body).to include product.display_price.to_s
      end
    end

    it "「一覧ページへ戻る」リンクが表示されていること" do
      expect(response.body).to include "一覧ページへ戻る"
    end
  end
end
