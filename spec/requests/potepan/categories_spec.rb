require 'rails_helper'

RSpec.describe "PotepanCategories", type: :request do
  let(:taxonomy) { create(:taxonomy, taxons: [taxon]) }
  let(:taxon) { create(:taxon, products: [product]) }
  let(:product) { create(:product) }

  before do
    get potepan_category_path(taxon.id)
  end

  describe "#show" do
    it "リクエストが成功すること" do
      expect(response).to have_http_status(200)
    end

    it "カテゴリーページへのリンクが表示されていること" do
      aggregate_failures do
        expect(response.body).to include taxonomy.name
        expect(response.body).to include taxon.name
      end
    end

    it "カテゴリーに属する商品が表示されていること" do
      expect(response.body).to include product.name
    end
  end
end
