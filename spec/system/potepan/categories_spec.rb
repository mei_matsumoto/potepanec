require 'rails_helper'

RSpec.describe "PotepanCategories", type: :system do
  let(:product1) { create(:product) }
  let(:product2) { create(:product) }
  let(:taxonomy) { create(:taxon, name: "Category", children: [taxon], products: [product2]) }
  let(:taxon) { create(:taxon, name: "T-shirt", products: [product1]) }

  it "カテゴリーページの表示", js: true do
    visit potepan_category_path(taxonomy.id)

    # taxonomyに分類される商品が表示される
    within '.productsContent' do
      expect(page).to have_link product1.name
      expect(page).to have_link product2.name
    end

    # カテゴリー名表示ヘッダー部分にSHOPリンクが表示されている
    within '.pageHeader' do
      expect(page).to have_link "SHOP"
    end

    # ドロップダウンメニューの操作
    within '.sideBar' do
      expect(page).to have_no_link taxon.name
      click_link taxonomy.name
      expect(page).to have_link taxon.name
      expect(page).to have_link taxon.products.count
      find(".collapseItem").click
    end

    # taxonに分類されない商品は表示されない
    within '.productsContent' do
      expect(page).to have_no_link product2.name
    end
  end

  it "商品詳細ページへの移動" do
    visit potepan_category_path(taxon.id)
    click_link product1.name

    within '.media-body' do
      expect(page).to have_content product1.name
    end
  end
end
