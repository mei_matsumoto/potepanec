require 'rails_helper'

RSpec.describe "PotepanProducts", type: :system do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:no_taxon_product) { create(:product) }

  context "商品がカテゴリーに属する場合" do
    before do
      visit potepan_product_path(product.id)
    end

    it "商品詳細ページの表示" do
      # 商品情報が表示される
      within '.media-body' do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end

      # 商品名表示ヘッダー部分にSHOPリンクは表示されていない
      within '.pageHeader' do
        expect(page).to have_no_link "SHOP"
      end
    end

    it "カテゴリー一覧ページへ戻る" do
      click_link "一覧ページへ戻る"

      # 商品が属するカテゴリーのページが表示される
      within '.sideBar' do
        expect(page).to have_link taxon.name
      end

      within '.productsContent' do
        expect(page).to have_link product.name
      end
    end
  end

  context "商品がカテゴリーに属さない場合" do
    before do
      visit potepan_product_path(no_taxon_product.id)
    end

    # カテゴリーページではなくtopページへ移動
    it "topページへ戻る" do
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_path
    end
  end
end
